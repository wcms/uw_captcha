<?php

/**
 * @file
 * uw_captcha.captcha.inc
 */

/**
 * Implements hook_captcha_default_points().
 */
function uw_captcha_captcha_default_points() {
  $export = array();

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node__form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node__form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_biblio_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node_biblio_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_contact_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_contact_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_uw_blog_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_uw_blog_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_uw_catalog_item_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node_uw_catalog_item_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_uw_ct_isr_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node_uw_ct_isr_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_uw_ct_person_profile_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_uw_ct_person_profile_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_uw_embedded_call_to_action_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node_uw_embedded_call_to_action_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_uw_embedded_facts_and_figures_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node_uw_embedded_facts_and_figures_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_uw_embedded_timeline_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node_uw_embedded_timeline_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_uw_event_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_uw_event_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_uw_home_page_banner_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_uw_home_page_banner_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_uw_image_gallery_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_uw_image_gallery_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_uw_news_item_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_uw_news_item_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_uw_opportunities_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node_uw_opportunities_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_uw_project_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_uw_project_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_uw_promotional_item_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_uw_promotional_item_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_uw_service_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_uw_service_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_uw_site_footer_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_uw_site_footer_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_uw_special_alert_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_uw_special_alert_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_uw_undergraduate_award_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node_uw_undergraduate_award_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_uw_web_form_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_uw_web_form_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_uw_web_page_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_uw_web_page_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_uwaterloo_custom_listing_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_uwaterloo_custom_listing_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_webform_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_webform_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'forward_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['forward_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'user_login';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['user_login'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'user_login_block';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['user_login_block'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'user_pass';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['user_pass'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'user_register_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['user_register_form'] = $captcha;

  return $export;
}
